#include <LiquidCrystal_I2C.h>

LiquidCrystal_I2C lcd(0x27, 16, 2);

#define STEP 2
#define SPEED 20

void setup() {
  pinMode(LED_1, OUTPUT);
}

void loop() {
  digitalWrite(SPEED, HIGH);
  delayMicroseconds(SPEED);
  digitalWrite(SPEED, LOW);
  delayMicroseconds(SPEED);
}